<?php

namespace Drupal\date_ymd\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;

/**
 * Plugin implementation of the 'date_ymd' widget.
 *
 * @FieldWidget(
 * id = "date_ymd",
 * label = @Translation("Date YMD"),
 * field_types = {
 * "date_ymd"
 * }
 * )
 */
class DateYMD extends WidgetBase implements WidgetInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $field_settings = $this->getFieldSettings();

    // Determine the beginning year.
    $begin = isset($field_settings['date_ymd_begin_year']) ? $field_settings['date_ymd_begin_year'] : 0;
    if (empty($begin) || !is_numeric($begin)) {
      $begin = 1900;
    }

    // Split up the current value.
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    preg_match('@(\d{4})(\d{2})(\d{2})@', $value, $match);

    // Define a fieldset wrapper.
    $element['date_ymd'] = [
      '#type' => 'fieldset',
      '#title' => $element['#title'],
      '#required' => $element['#required'],
      '#attributes' => ['class' => ['container-inline']],
    ];

    $element['date_ymd']['year'] = [
      '#type' => 'select',
      '#options' => $this->yearOptions($begin),
      '#empty_option' => ['0000' => '--'],
      '#default_value' => isset($match[1]) ? $match[1] : '',
    ];

    $element['date_ymd']['month'] = [
      '#type' => 'select',
      '#options' => $this->monthOptions(),
      '#default_value' => (isset($match[2]) ? $match[2] : ''),
      '#empty_option' => ['00' => '--'],
    ];

    $element['date_ymd']['day'] = [
      '#type' => 'select',
      '#options' => $this->dayOptions(),
      '#default_value' => (isset($match[3]) ? $match[3] : ''),
      '#empty_option' => ['00' => '--'],
    ];

    return $element;
  }

  /**
   * Utility function to provide year options.
   */
  private function yearOptions($begin) {
    $options = [];
    foreach (range($begin, date('Y')) as $year) {
      $options[$year] = $year;
    }
    return $options;
  }

  /**
   * Utility function to provide month options.
   */
  private function monthOptions() {
    $options = [];
    foreach (range(1, 12) as $month) {
      $options[sprintf('%02s', $month)] = format_date(gmmktime(0, 0, 0, $month, 2, 1970), 'custom', 'F', NULL);
    }
    return $options;
  }

  /**
   * Utility function to provide day options.
   */
  private function dayOptions() {
    $options = [];
    foreach (range(1, 31) as $day) {
      $options[sprintf('%02s', $day)] = $day;
    }
    return $options;
  }

}
