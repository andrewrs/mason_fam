This module changes fields in various ways.

It can resize, in an update function:

\Drupal::service('druidfire')->kaboom('resize', 'paragraph', 'section_title', 1024);

Or 

drush druidfire resize paragraph section_title 1024

If you want to resize something else than the main property:

\Drupal::service('druidfire')->kaboom('resize', 'paragraph', 'link', 1024, 'title');

You can convert a non-formatted field to a formatted one:

\Drupal::service('druidfire')->kaboom('string2formatted', 'paragraph', 'component_flora_answer');

