<?php

namespace Drupal\druidfire;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

class Druidfire {

  /**
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  /**
   * @var \Drupal\Core\Database\Schema
   */
  protected $schema;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyvalue;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  public function __construct(StorageInterface $configStorage, Connection $connection, EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager, KeyValueFactoryInterface $keyValueFactory, MessengerInterface $messenger) {
    $this->configStorage = $configStorage;
    $this->schema = $connection->schema();
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->keyvalue = $keyValueFactory->get('entity.storage_schema.sql');
    $this->messenger = $messenger;
  }

  public function kaboom($prefix, $entityTypeId, $fieldName, ...$args) {
    $n = count($args);
    if ($n && !is_numeric($args[$n -1])) {
      $propertyName = array_pop($args);
    }
    $columnName = $this->getColumnName($entityTypeId, $fieldName, $propertyName ?? NULL);
    $key = "$entityTypeId.field_schema_data.$fieldName";
    $callable = [$this, $prefix . 'Schema'];
    $schema = $this->keyvalue->get($key, []);
    foreach (array_keys($schema) as $tableName) {
      try {
        $this->messenger->addMessage("Changing the schema of $tableName $columnName");
        $schema = $callable($schema, $tableName, $columnName, ...$args);
      }
      catch (\Exception $e) {
        $this->messenger->addWarning("Schema change failed " . $e->getMessage());
      }
    }
    $this->keyvalue->set($key, $schema);
    $this->changeConfig($entityTypeId, $fieldName, $prefix, 'storage', $args);
    $this->changeConfig($entityTypeId, $fieldName, $prefix, 'field', $args);
    $this->entityTypeManager->clearCachedDefinitions();
  }

  /**
   * @param $entityTypeId
   * @param $fieldName
   *
   * @return string
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\Sql\SqlContentEntityStorageException
   */
  protected function getColumnName($entityTypeId, $fieldName, $propertyName) {
    $entityStorage = $this->entityTypeManager->getStorage($entityTypeId);
    $mapping = $entityStorage->getTableMapping();
    $definitions = $this->entityFieldManager->getFieldStorageDefinitions($entityTypeId);
    if (!isset($definitions[$fieldName]) || !$mapping instanceof DefaultTableMapping || !$entityStorage instanceof SqlContentEntityStorage) {
      throw new \InvalidArgumentException('Can only change default SQL fields.');
    }
    $definition = $definitions[$fieldName];
    return $mapping->getFieldColumnName($definition, $propertyName ?: $definition->getMainPropertyName());
  }

  /**
   * @param $entityTypeId
   * @param $fieldName
   * @param $prefix
   * @param $configType
   *   Can be storage or field.
   * @param $args
   */
  protected function changeConfig($entityTypeId, $fieldName, $prefix, $configType, $args) {
    $callable = [$this, $prefix . 'Field' . ucfirst($configType)];
    $configNames = $this->configStorage->listAll("field.$configType.$entityTypeId.");
    $configNames = preg_grep("/\\.$fieldName$/", $configNames);
    $this->doChangeConfig($callable, $configNames, $args);
    if ($configType === 'field') {
      array_unshift($args, $fieldName);
      $callable[1] = $prefix . 'FormDisplay';
      $this->doChangeConfig($callable, $configNames, $args, "core.entity_form_display.$entityTypeId.");
      $callable[1] = $prefix . 'ViewDisplay';
      $this->doChangeConfig($callable, $configNames, $args, "core.entity_view_display.$entityTypeId.");
    }
  }

  /**
   * @param $callable
   * @param array $configNames
   * @param array $args
   * @param string $new_prefix
   *   This will be core.entity_form_display.$entityTypeId. or
   *   core.entity_view_display.$entityTypeId.
   */
  protected function doChangeConfig($callable, array $configNames, array $args, $new_prefix = '') {
    if (!is_callable($callable)) {
      return;
    }
    foreach ($configNames as $configName) {
      if ($new_prefix) {
        // This extracts the bundle from the field config name which is
        // field(0).field(1).node(2).nodetype(3).fieldname(4)
        $actualConfigNames = $this->configStorage->listAll($new_prefix . explode('.', $configName)[3] . '.');
      }
      else {
        $actualConfigNames = [$configName];
      }
      foreach ($actualConfigNames as $actualConfigName) {
        if ($yaml = $this->configStorage->read($actualConfigName)) {
          $this->messenger->addMessage("Changing config $actualConfigName");
          $yaml = $callable($yaml, ...$args);
          $this->configStorage->write($actualConfigName, $yaml);
        }
      }
    }
  }

  protected function resizeSchema(array $schema, $tableName, string $columnName, $size) {
    $schema[$tableName]['fields'][$columnName]['length'] = $size;
    $this->schema->changeField($tableName, $columnName, $columnName, $schema[$tableName]['fields'][$columnName]);
    return $schema;
  }

  protected function resizeFieldStorage(array $yaml, $size) {
    $yaml['settings']['max_length'] = $size;
    return $yaml;
  }

  protected function string2formattedSchema($schema, $tableName, string $columnName) {
    $spec = [
      'type' => 'text',
      'size' => 'big',
    ];
    $schema[$tableName]['fields'][$columnName] = $spec;
    $this->schema->changeField($tableName, $columnName, $columnName, $spec);
    $format = preg_replace('/_value$/', '_format', $columnName);
    $schema[$tableName]['fields'][$format] = [
      'type' => 'varchar_ascii',
      'length' => 255,
    ];
    $schema[$tableName]['indexes'][$format] = [$format];
    $this->schema->addField($tableName, $format, $schema[$tableName]['fields'][$format]);
    $this->schema->addIndex($tableName, $format, $schema[$tableName]['indexes'][$format], $schema[$tableName]);
    return $schema;
  }

  protected function string2formattedFieldStorage(array $yaml) {
    $yaml['type'] = 'text_long';
    return $yaml;
  }

  protected function string2formattedFieldField(array $yaml) {
    $yaml['field_type'] = 'text_long';
    return $yaml;
  }

  protected function string2formattedFormDisplay(array $yaml, $fieldName) {
    $yaml['content'][$fieldName]['type'] = 'text_textarea';
    unset($yaml['content'][$fieldName]['settings']['size']);
    $yaml['content'][$fieldName]['settings']['rows'] = 9;
    return $yaml;
  }

}
