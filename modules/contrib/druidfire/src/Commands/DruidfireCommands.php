<?php

namespace Drupal\druidfire\Commands;


use Drush\Commands\DrushCommands;

class DruidfireCommands extends DrushCommands {

  /**
   * Change field length.
   *
   * @command druidfire:resize
   *
   * @param $entityTypeId The entity type id.
   * @param $fieldName The field name. (Best works for text fields.)
   * @param $size The new size.
   * @param $property Optional property.
   */
  public function resize($entityTypeId, $fieldName, $size, $property = NULL) {
    \Drupal::service('druidfire')->kaboom('resize', $entityTypeId, $fieldName, $size, $property);
  }

  /**
   * Change a string field to a formatted field.
   *
   * @command druidfire:string2formatted
   *
   * @param $entityTypeId The entity type id.
   * @param $fieldName The field name. (Best works for text fields.)
   */
  public function string2formatted($entityTypeId, $fieldName) {
    \Drupal::service('druidfire')->kaboom('string2formatted', $entityTypeId, $fieldName);
  }

}
