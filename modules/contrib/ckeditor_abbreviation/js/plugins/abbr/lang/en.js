/**
 * @file
 * The abbr language string definitions.
 */

"use strict"

CKEDITOR.plugins.setLang('abbr', 'en', {
  buttonTitle: 'Insert Glossary Term',
  menuItemTitle: 'Edit Glossary Term',
  dialogTitle: 'Glossary Term Properties',
  dialogAbbreviationTitle: 'Term',
  dialogExplanationTitle: 'Definition'
});

