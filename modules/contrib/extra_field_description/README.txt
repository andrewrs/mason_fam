README.txt
==========

When we create content, it does not always work how we want.
Sometimes we need clarify field below and above of the field.

You can configure extra description for each field in Content type Manage Form
Display settings(/admin/structure/types/manage/article/form-display).

* Features *
Optimize for Rubik theme.
Append other html tags.

AUTHOR/MAINTAINER
======================
Samvel Akopyan(https://www.drupal.org/u/samvel)
Yogesh Pawar(https://www.drupal.org/u/yogesh-pawar)
